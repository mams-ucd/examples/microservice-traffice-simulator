import org.junit.Before;
import org.junit.Test;

import hypermats.core.model.Action;
import hypermats.core.model.AgentDescription;
import hypermats.core.model.VehicleType;
import street.model.StreetConfiguration;
import street.model.Vehicle;
import street.model.VehicleDescription;
import street.simulation.Lane;
import street.simulation.StreetSimulation;

public class LaneTest {
    private StreetSimulation simulation;
    private StreetConfiguration configuration;

    @Before public void setup() {
        configuration = new StreetConfiguration("s1", 19.5);
        simulation = new StreetSimulation(configuration, "localhost");

    }
    @Test public void laneTest() {
        try {
            VehicleDescription vehicleDescription1 = new VehicleDescription();
            vehicleDescription1.name = "v1";
            vehicleDescription1.agentDescription = new AgentDescription();
            vehicleDescription1.agentDescription.name = "rem";
            vehicleDescription1.agentDescription.webhook = "localhost";

            VehicleDescription vehicleDescription2 = new VehicleDescription();
            vehicleDescription2.name = "v2";
            vehicleDescription2.agentDescription = new AgentDescription();
            vehicleDescription2.agentDescription.name = "bob";
            vehicleDescription2.agentDescription.webhook = "localhost";
            
            VehicleType type = new VehicleType("http://localhost/types/basic", "basic", 1.0);

            Vehicle vehicle = new Vehicle(vehicleDescription1, type, "localhost");
            vehicle.setSimulation(simulation);
            Vehicle vehicle2 = new Vehicle(vehicleDescription2, type, "localhost");
            vehicle2.setSimulation(simulation);

            Lane lane = new Lane(configuration.length);

            lane.join(vehicle);
            System.out.println("2nd car: " + lane.join(vehicle2));
            System.out.println(lane.toString());
            // vehicle.getAgent().setAction(new Action("accelerate"));
            // lane.step(5);
            // System.out.println(lane.toString());
            // lane.join(vehicle2);
            // vehicle.getAgent().setAction(new Action("skip"));
            // vehicle2.getAgent().setAction(new Action("accelerate"));
            // System.out.println(lane.toString());
            // lane.step(5);
            // System.out.println(lane.toString());
    
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
    @Test public void oneCarTest() {
        try {
            VehicleDescription vehicleDescription1 = new VehicleDescription();
            vehicleDescription1.name = "v1";
            vehicleDescription1.agentDescription = new AgentDescription();
            vehicleDescription1.agentDescription.name = "rem";
            vehicleDescription1.agentDescription.webhook = "localhost";

            VehicleType type = new VehicleType("http://localhost/types/basic", "basic", 1.0);

            Vehicle vehicle = new Vehicle(vehicleDescription1, type, "localhost");
            vehicle.setSimulation(simulation);

            Lane lane = new Lane(configuration.length);

            lane.join(vehicle);
            System.out.println(lane.toString());
            vehicle.agent.setAction(new Action("accelerate"));
            lane.step(5);
            System.out.println(lane.toString());
            vehicle.agent.setAction(new Action("skip"));
            lane.step(5);
            System.out.println(lane.toString());
            // lane.join(vehicle2);
            // vehicle.getAgent().setAction(new Action("skip"));
            // vehicle2.getAgent().setAction(new Action("accelerate"));
            // System.out.println(lane.toString());
            // lane.step(5);
            // System.out.println(lane.toString());
    
        } catch (Throwable th) {
            th.printStackTrace();
        }
    }
}
