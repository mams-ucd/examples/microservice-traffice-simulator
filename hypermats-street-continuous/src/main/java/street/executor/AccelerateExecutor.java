package street.executor;

import street.model.Vehicle;

public class AccelerateExecutor implements Executor {
    @Override
    public String getActionId() {
        return "accelerate";
    }

    @Override
    public Vehicle execute(Vehicle vehicle) {
        vehicle.speed = vehicle.speed+10;
        return vehicle;
    }
    
}
