package street.model;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import hypermats.core.model.Agent;
import hypermats.core.model.VehicleType;
import street.simulation.StreetSimulation;

public class Vehicle {
    private static ObjectMapper mapper = new ObjectMapper();

    private StreetSimulation simulation;
    public Agent agent;
    /**
     * Total distance travelled along the street
     */
    public double totalDistanceTravelled = 0;

    /**
     * Speed in kph
     */
    public int speed = 0;

    /**
     * Vehicle Type
     */
    private VehicleType type;

    /**
     * Vehicle Id
     */
    public String name;

    /**
     * Vehicle @id
     */
    public String url;

    /**
     * Distance travelled in this tick
     */
    public double distanceTravelled = 0.0;

    /**
     * Unused Distance to travel in this tick
     */
    public double distanceToTravel = 0.0;

    public Vehicle(VehicleDescription vehicleDescription, VehicleType type, String host) {
        agent =  new Agent(vehicleDescription.agentDescription);
        this.type = type;
        name = vehicleDescription.name;
        url = "http://" + host + "/vehicles/"+name;
    }

    public JsonNode toJson() {
        ObjectNode typeNode = mapper.createObjectNode();
        typeNode.put("@id", type.url);
        typeNode.put("@type", "VehicleType");
        typeNode.put("type", type.type);
        typeNode.put("length", type.length);

        ObjectNode state = mapper.createObjectNode();
        // state.put("@id", url+"/state");
        state.put("@type", "ContinuousStreetState");
        state.put("totalDistanceTravelled", totalDistanceTravelled);
        state.put("distanceTravelled", distanceTravelled);
        state.put("distanceToTravel", distanceToTravel);
        state.put("speed", speed);

        ArrayNode affordances = mapper.createArrayNode();
        ObjectNode skip = mapper.createObjectNode();
        skip.put("id", "skip");
        skip.set("parameters", mapper.createArrayNode());
        affordances.add(skip);
        if (speed == 0) {
            ObjectNode accelerate = mapper.createObjectNode();
            accelerate.put("id", "accelerate");
            accelerate.set("parameters", mapper.createArrayNode());
            affordances.add(accelerate);
        } else {
            ObjectNode brake = mapper.createObjectNode();
            brake.put("id", "brake");
            brake.set("parameters", mapper.createArrayNode());
            affordances.add(brake);
        }
        
        ObjectNode action = mapper.createObjectNode();
        action.put("@id",url+"/action");
        action.put("id", agent.getAction().id);
        ArrayNode parameters = mapper.createArrayNode();
        if (agent.getAction().parameters != null) {
            for (Object parameter:agent.getAction().parameters) {
                parameters.addPOJO(parameter);
            }
        }
        action.set("parameters", parameters);

        ObjectNode agentNode = mapper.createObjectNode();
        agentNode.put("@type", "Agent");
        agentNode.put("name", agent.getAgent().name);
        agentNode.put("webhook", agent.getAgent().webhook);

        ObjectNode vehicle = mapper.createObjectNode();
        vehicle.put("@id", url);
        vehicle.put("@context", "http://ont.astralanguage.com/2023/01/simulation");
        vehicle.put("@type", "Vehicle");
        vehicle.put("name", name);
        vehicle.set("type", typeNode);
        vehicle.set("location", simulation.toEmbeddedJson());
        vehicle.set("state", state);
        vehicle.set("agent", agentNode);
        vehicle.set("action", action);
        vehicle.set("affordances", affordances);
        return vehicle;
    }

    public StreetSimulation getSimulation() {
        return simulation;
    }

    public void remove() {
        simulation.removeVehicle(this);
    }

    public boolean atRoadStart() {
        return totalDistanceTravelled == 0;
    }

    public double getLength() {
        return type.length;
    }

    public String toString() {
        return toJson().toPrettyString();
    }

    public void setSimulation(StreetSimulation simulation) {
        this.simulation = simulation;
    }

    public double speedInMps() {
        return Math.round(speed * 100 / 3.6) / 100.0;
    }
}
