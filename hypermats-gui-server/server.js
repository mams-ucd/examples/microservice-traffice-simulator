const http = require('http');
const WebSocketServer = require('websocket').server;

const server = http.createServer();
server.listen(9898);

const wsServer = new WebSocketServer({
    httpServer: server
});

/**
 * This server connects the HyperMATS GUI front-end code with the 
 * HyperMATS servers and simulation.
 * 
 * It accepts connections from: 
 * 
 * 1. The GUI client
 *   This establishes a connection and sends a dump of the road data.
 * 
 *   Road data format:
 * 
 *   { "type":"data",
 *     "body": //Road data}
 * 
 *   It can also send a message to the server asking it to start the simulation.
 *   It can also send a message asking for the simulation to be stopped.
 * 
 * 2. The HyperMATS server
 *   Once the "Start Simulation" button has been pressed, 
 *   It is sent the message to start the simulation, and stop the simulation.
 *   It returns data about the agent name and location to the /agent endpoint
 *   which is forwarded to the GUI client
 * 
 * TODO: Remove co-ordinates from ROAD DATA
 * Just send length and id
 * Accept back agent Id, road/junction ID and length-along and work this out in terms
 * of co-ordinates
 */ 

var roadData;

wsServer.on('request', function(request) {
    const connection = request.accept(null, request.origin);

    function send(message) {
        connection.sendUTF(message);
    }
    connection.on('message', function(message) {
      
        console.log('Received Message:', message.utf8Data);
        var messageJson = JSON.parse(message.utf8Data);
        
        if (messageJson.type == "stop") {
            console.log("Stopping");
        }
        if (messageJson.type == "data") {
            //Get data object, cache for sending to server
            console.log("Getting data: ", messageJson["body"]);
            roadData = messageJson["body"];
        }
    });

    server.on("request", (req, res) => {
        console.log(req.url);
        let url = req.url;
        if (url == "/agent") {
                console.log("Handling agent code");
                req.on('data', function (chunk) {
                    console.log('BODY: ' + chunk);
                    connection.sendUTF(chunk);
                    res.end("recieved");
                });
        } else if (url == "/data") {
                req.on('data', function (chunk) {
                    if (roadData) {
                        res.end(JSON.stringify(roadData));
                    } else {
                        res.end("Not ready yet");
                    }
                    
                });
        } else {
            res.end('404');
            console.log("Client requested page not found - 404 error");
        }
    });

    connection.on('close', function(reasonCode, description) {
        console.log('Client has disconnected.');
    });
});


/*


curl -X POST http://localhost:9898/agent \
   -H 'Content-Type: application/json' \
   -d '{"agent":"agentZero","id":"14654","distance":"1"}'

curl -X POST http://localhost:9898/agent \
   -H 'Content-Type: application/json' \
   -d '{"agent":"agentZero","id":"14654","distance":"2"}'

curl -X POST http://localhost:9898/agent \
   -H 'Content-Type: application/json' \
   -d '{"agent":"agentZero","id":"14654","distance":"3"}'

curl -X POST http://localhost:9898/agent \
   -H 'Content-Type: application/json' \
   -d '{"agent":"agentZero","id":"14654","distance":"4"}'


   curl -X POST http://localhost:9898/data \
   -H 'Content-Type: application/json' \
   -d '{}'


   */
