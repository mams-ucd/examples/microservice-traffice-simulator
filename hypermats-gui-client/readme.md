# OpenLayers + Vite

This example demonstrates how the `ol` package can be used with [Vite](https://vitejs.dev/).

## Installing

Dependencies: npm 9.3.0

To install run: `npm install` 

## Running

Make sure the HyperMATS gui server is running

Then to run: `npm start`

Navigate to http://127.0.0.1:5173/

The client will register with the server, then to display the agent, send curl requests to the server (detailed in server README)

To generate a build ready for production:

 `npm run build`

Then deploy the contents of the `dist` directory to your server.  You can also run `npm run serve` to serve the results of the `dist` directory for preview.
