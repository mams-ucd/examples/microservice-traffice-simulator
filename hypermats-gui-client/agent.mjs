export class Agent {
    constructor(agentID, lat, lon) {
        this.id = agentID;
        this.lat = lat;
        this.lon = lon;
    }
}
