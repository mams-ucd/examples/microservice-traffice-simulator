package junction.model;

import hypermats.core.model.Agent;
import hypermats.core.model.AgentDescription;
import hypermats.core.model.Link;
import junction.simulation.JunctionSimulation;

public class Vehicle extends Agent {
    private JunctionSimulation simulation;
    private int queue;
    private int speed = 1;

    public Vehicle(JunctionSimulation simulation, int queue, AgentDescription agent) {
        super(agent);
        this.simulation = simulation;
        this.queue = queue;
    }

    public VehicleState getState(String host) {
        VehicleState state = new VehicleState();
        state.type = "junction";
        state.url = simulation.getUrl(host);
        state.queue = queue;
        state.at = simulation.getVehicleQueue(queue).atLocation(this);
        for (Link link : simulation.getOutLinks()) {
            if (link.url != null && !link.url.equals("")) state.exits.add(link.url);
        }
        state.speed = speed;
        return state;
    }
}
