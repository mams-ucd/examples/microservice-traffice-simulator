package config.model;

public class Registration {
    public String name;
    public String url;

    public Registration(String name, String url) {
        this.name = name;
        this.url = url;
    }

    public Registration() {}
}
