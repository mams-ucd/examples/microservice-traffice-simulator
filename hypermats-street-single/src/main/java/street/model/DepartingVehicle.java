package street.model;

import java.net.URI;

public class DepartingVehicle {
    public String name;
    public URI uri;

    public DepartingVehicle(String name, URI uri) {
        this.name = name;
        this.uri = uri;
    }
}