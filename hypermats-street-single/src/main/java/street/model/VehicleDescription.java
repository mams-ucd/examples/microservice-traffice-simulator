package street.model;

import hypermats.core.model.Action;

public class VehicleDescription {
    public VehicleState perception;
    public Action action;

    public VehicleDescription(VehicleState perception, Action action) {
        this.perception = perception;
        this.action = action;
    }
}
