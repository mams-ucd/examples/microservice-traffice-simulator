package street.executor;

import street.model.Vehicle;

public class StopExecutor implements Executor {
    @Override
    public String getActionId() {
        return "stop";
    }

    @Override
    public Vehicle execute(Vehicle[] positions, int index) {
        positions[index].setSpeed(0);
        return null;
    }
    
}
