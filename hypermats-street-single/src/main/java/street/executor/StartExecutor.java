package street.executor;

import street.model.Vehicle;

public class StartExecutor implements Executor {
    @Override
    public String getActionId() {
        return "start";
    }

    @Override
    public Vehicle execute(Vehicle[] positions, int index) {
        positions[index].setSpeed(1);
        return null;
    }
    
}
