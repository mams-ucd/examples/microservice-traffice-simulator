package agent.controller;

import java.net.InetAddress;
import java.net.URI;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;

import agent.model.Agent;
import hypermats.core.model.AgentDescription;

@RestController
public class AgentController {
    private Map<String, Agent> agents = new HashMap<>();

    @Value("${server.port}")
    private int port;
    
    @PutMapping("/agents/iteration")
    public ResponseEntity<String> configuration(@RequestBody JsonNode configuration) {
        System.out.println("Received Iteration");
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    
    @PostMapping("/agents")
    public ResponseEntity<String> addAgent(@RequestBody JsonNode agentBody) {
        String name = agentBody.get("name").asText();
        String url = agentBody.get("init").get("url").asText();

        Agent agent = new Agent(name);
        agents.put(name, agent);
        AgentDescription description = null;
        try {
            description = agent.getAgentDescription(getHost());
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
        
        RestTemplate template = new RestTemplate();
        URI location = template.postForLocation(url, description);
        agent.setUri(location);
        
        return ResponseEntity.
                    status(HttpStatus.CREATED).
                    header(HttpHeaders.LOCATION, description.webhook).
                    build();
    }

    @PutMapping("/agents/{name}")
    public ResponseEntity<String> execute(@PathVariable String name, @RequestBody JsonNode perceptions) {
        Agent agent = agents.get(name);
        if (agent == null) {
            return ResponseEntity.
                status(HttpStatus.NOT_FOUND).
                build();
        }
        
        agent.execute(perceptions);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    private String getHost() throws UnknownHostException {
        return InetAddress.getLocalHost().getHostAddress() + ":" + port;
    }
}
