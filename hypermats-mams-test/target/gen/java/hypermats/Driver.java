package hypermats;
/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;

import com.fasterxml.jackson.databind.JsonNode;
import mams.web.HttpResponse;
import mams.*;

public class Driver extends ASTRAClass {
	public Driver() {
		setParents(new Class[] {mams.PassiveMAMSAgent.class});
		addRule(new Rule(
			"hypermats.Driver", new int[] {12,9,12,40},
			new GoalEvent('+',
				new Goal(
					new Predicate("main", new Term[] {
						new ListTerm(new Term[] {
							new Funct("init", new Term[] {
								new Variable(new ObjectType(JsonNode.class), "init",false)
							})
						})
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"hypermats.Driver", new int[] {12,39,36,5},
				new Statement[] {
					new ScopedSubgoal(
						"hypermats.Driver", new int[] {13,8,36,5},
						"MAMSAgent",
						new Goal(
							new Predicate("init", new Term[] {})
						)
					),
					new ScopedSubgoal(
						"hypermats.Driver", new int[] {14,8,36,5},
						"MAMSAgent",
						new Goal(
							new Predicate("created", new Term[] {
								Primitive.newPrimitive("base")
							})
						)
					),
					new ModuleCall("cartago",
						"hypermats.Driver", new int[] {15,8,15,59},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("Created Driver: "),
								new ModuleTerm("system", Type.STRING,
									new Predicate("name", new Term[] {}),
									new ModuleTermAdaptor() {
										public Object invoke(Intention intention, Predicate predicate) {
											return ((astra.lang.System) intention.getModule("hypermats.Driver","system")).name(
											);
										}
										public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
											return ((astra.lang.System) visitor.agent().getModule("hypermats.Driver","system")).name(
											);
										}
									}
								)
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("hypermats.Driver","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"hypermats.Driver", new int[] {16,8,16,40},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("init: "),
								new Variable(new ObjectType(JsonNode.class), "init")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("hypermats.Driver","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new Declaration(
						new Variable(Type.STRING, "service_uri"),
						"hypermats.Driver", new int[] {18,8,36,5},
						new ModuleTerm("converter", Type.STRING,
							new Predicate("valueAsString", new Term[] {
								new Variable(new ObjectType(JsonNode.class), "init"),
								Primitive.newPrimitive("/url")
							}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((mams.JSONConverter) intention.getModule("hypermats.Driver","converter")).valueAsString(
										(com.fasterxml.jackson.databind.JsonNode) intention.evaluate(predicate.getTerm(0)),
										(java.lang.String) intention.evaluate(predicate.getTerm(1))
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONConverter) visitor.agent().getModule("hypermats.Driver","converter")).valueAsString(
										(com.fasterxml.jackson.databind.JsonNode) visitor.evaluate(predicate.getTerm(0)),
										(java.lang.String) visitor.evaluate(predicate.getTerm(1))
									);
								}
							}
						)
					),
					new Declaration(
						new Variable(new ObjectType(JsonNode.class), "state"),
						"hypermats.Driver", new int[] {21,8,36,5},
						new ModuleTerm("builder", new ObjectType(com.fasterxml.jackson.databind.JsonNode.class),
							new Predicate("createObject", new Term[] {}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((mams.JSONBuilder) intention.getModule("hypermats.Driver","builder")).createObject(
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONBuilder) visitor.agent().getModule("hypermats.Driver","builder")).createObject(
									);
								}
							}
						)
					),
					new ScopedSubgoal(
						"hypermats.Driver", new int[] {22,8,36,5},
						"PassiveMAMSAgent",
						new Goal(
							new Predicate("itemResource", new Term[] {
								Primitive.newPrimitive("state"),
								new Variable(new ObjectType(JsonNode.class), "state")
							})
						)
					),
					new Query(
						"hypermats.Driver", new int[] {25,8,25,78},
						new Predicate("artifact", new Term[] {
							Primitive.newPrimitive("state"),
							new Variable(Type.STRING, "qualified_name",false),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						})
					),
					new ModuleCall("cartago",
						"hypermats.Driver", new int[] {26,8,26,49},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("getUri", new Term[] {
								new Variable(Type.STRING, "uri",false)
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("hypermats.Driver","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new Declaration(
						new Variable(new ObjectType(JsonNode.class), "description"),
						"hypermats.Driver", new int[] {31,8,36,5},
						new ModuleTerm("builder", new ObjectType(com.fasterxml.jackson.databind.JsonNode.class),
							new Predicate("createObject", new Term[] {}),
							new ModuleTermAdaptor() {
								public Object invoke(Intention intention, Predicate predicate) {
									return ((mams.JSONBuilder) intention.getModule("hypermats.Driver","builder")).createObject(
									);
								}
								public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
									return ((mams.JSONBuilder) visitor.agent().getModule("hypermats.Driver","builder")).createObject(
									);
								}
							}
						)
					),
					new ModuleCall("builder",
						"hypermats.Driver", new int[] {32,8,32,63},
						new Predicate("addProperty", new Term[] {
							new Variable(new ObjectType(JsonNode.class), "description"),
							Primitive.newPrimitive("name"),
							new ModuleTerm("system", Type.STRING,
								new Predicate("name", new Term[] {}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.System) intention.getModule("hypermats.Driver","system")).name(
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.System) visitor.agent().getModule("hypermats.Driver","system")).name(
										);
									}
								}
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((mams.JSONBuilder) intention.getModule("hypermats.Driver","builder")).addProperty(
									(com.fasterxml.jackson.databind.JsonNode) intention.evaluate(predicate.getTerm(0)),
									(java.lang.String) intention.evaluate(predicate.getTerm(1)),
									(java.lang.String) intention.evaluate(predicate.getTerm(2))
								);
							}
						}
					),
					new ModuleCall("builder",
						"hypermats.Driver", new int[] {33,8,33,56},
						new Predicate("addProperty", new Term[] {
							new Variable(new ObjectType(JsonNode.class), "description"),
							Primitive.newPrimitive("webhook"),
							new Variable(Type.STRING, "uri")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return true;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((mams.JSONBuilder) intention.getModule("hypermats.Driver","builder")).addProperty(
									(com.fasterxml.jackson.databind.JsonNode) intention.evaluate(predicate.getTerm(0)),
									(java.lang.String) intention.evaluate(predicate.getTerm(1)),
									(java.lang.String) intention.evaluate(predicate.getTerm(2))
								);
							}
						}
					),
					new ModuleCall("cartago",
						"hypermats.Driver", new int[] {34,8,34,71},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("POST: "),
								Operator.newOperator('+',
									new Variable(new ObjectType(JsonNode.class), "description"),
									Operator.newOperator('+',
										Primitive.newPrimitive(" to: "),
										new Variable(Type.STRING, "service_uri")
									)
								)
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("hypermats.Driver","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"hypermats.Driver", new int[] {38,9,38,78},
			new ModuleEvent("cartago",
				"$cse",
				new Predicate("signal", new Term[] {
					new Variable(Type.STRING, "source_artifact_name",false),
					new Funct("update", new Term[] {
						new Variable(new ObjectType(JsonNode.class), "node",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("hypermats.Driver","cartago")).signal(
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"hypermats.Driver", new int[] {38,77,41,5},
				new Statement[] {
					new ModuleCall("cartago",
						"hypermats.Driver", new int[] {39,8,39,58},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("source: "),
								new Variable(Type.STRING, "source_artifact_name")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("hypermats.Driver","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"hypermats.Driver", new int[] {40,8,40,42},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("UPDATE: "),
								new Variable(new ObjectType(JsonNode.class), "node")
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("hypermats.Driver","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		fragment.addModule("system",astra.lang.System.class,agent);
		fragment.addModule("builder",mams.JSONBuilder.class,agent);
		fragment.addModule("converter",mams.JSONConverter.class,agent);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new TestSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Driver().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
