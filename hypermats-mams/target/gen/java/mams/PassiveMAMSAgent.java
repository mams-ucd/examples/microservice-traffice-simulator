package mams;
/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;

import com.fasterxml.jackson.databind.JsonNode;

public class PassiveMAMSAgent extends ASTRAClass {
	public PassiveMAMSAgent() {
		setParents(new Class[] {MAMSAgent.class});
		addRule(new Rule(
			"mams.PassiveMAMSAgent", new int[] {8,9,8,130},
			new GoalEvent('+',
				new Goal(
					new Predicate("itemResource", new Term[] {
						new Variable(Type.STRING, "name",false),
						new Variable(new ObjectType(JsonNode.class), "node",false)
					})
				)
			),
			new AND(
				new NOT(
					new Predicate("have", new Term[] {
						new Variable(Type.STRING, "name")
					})
				),
				new Predicate("artifact", new Term[] {
					Primitive.newPrimitive("base"),
					new Variable(Type.STRING, "baseName",false),
					new Variable(new ObjectType(cartago.ArtifactId.class), "baseId",false)
				})
			),
			new Block(
				"mams.PassiveMAMSAgent", new int[] {8,129,17,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.STRING, "resName"),
						"mams.PassiveMAMSAgent", new int[] {9,8,17,5},
						Operator.newOperator('+',
							new Variable(Type.STRING, "baseName"),
							Operator.newOperator('+',
								Primitive.newPrimitive("-"),
								new Variable(Type.STRING, "name")
							)
						)
					),
					new ModuleCall("cartago",
						"mams.PassiveMAMSAgent", new int[] {10,8,10,128},
						new Predicate("makeArtifact", new Term[] {
							new Variable(Type.STRING, "resName"),
							Primitive.newPrimitive("mams.artifacts.PassiveItemArtifact"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.STRING, "name"),
										new Variable(new ObjectType(JsonNode.class), "node")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("mams.PassiveMAMSAgent","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("mams.PassiveMAMSAgent","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.PassiveMAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.PassiveMAMSAgent", new int[] {11,8,11,50},
						new Predicate("linkArtifacts", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							Primitive.newPrimitive("out-1"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "baseId")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.PassiveMAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.PassiveMAMSAgent", new int[] {12,8,12,25},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.PassiveMAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.PassiveMAMSAgent", new int[] {13,8,13,44},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("createRoute", new Term[] {})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.PassiveMAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.PassiveMAMSAgent", new int[] {14,8,17,5},
						new Predicate("artifact", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(Type.STRING, "resName"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					),
					new ModuleCall("cartago",
						"mams.PassiveMAMSAgent", new int[] {16,8,16,49},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("getUri", new Term[] {
								new Variable(Type.STRING, "uri",false)
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.PassiveMAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.PassiveMAMSAgent", new int[] {19,9,19,71},
			new GoalEvent('+',
				new Goal(
					new Predicate("monitorPassiveItem", new Term[] {
						new Variable(Type.STRING, "name",false),
						new Variable(Type.STRING, "type",false),
						new Variable(Type.STRING, "qname",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"mams.PassiveMAMSAgent", new int[] {19,70,25,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.PassiveMAMSAgent", new int[] {20,8,20,60},
						new Predicate("lookupArtifact", new Term[] {
							new Variable(Type.STRING, "qname"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.PassiveMAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.PassiveMAMSAgent", new int[] {21,8,21,25},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.PassiveMAMSAgent","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.PassiveMAMSAgent", new int[] {24,8,25,5},
						new Predicate("artifact", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(Type.STRING, "qname"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new TestSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new PassiveMAMSAgent().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
