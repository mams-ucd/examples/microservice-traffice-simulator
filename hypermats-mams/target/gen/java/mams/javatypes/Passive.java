package mams.javatypes;
/**
 * GENERATED CODE - DO NOT CHANGE
 */

import astra.core.*;
import astra.execution.*;
import astra.event.*;
import astra.messaging.*;
import astra.formula.*;
import astra.lang.*;
import astra.statement.*;
import astra.term.*;
import astra.type.*;
import astra.tr.*;
import astra.reasoner.util.*;


public class Passive extends ASTRAClass {
	public Passive() {
		setParents(new Class[] {mams.MAMSAgent.class});
		addRule(new Rule(
			"mams.javatypes.Passive", new int[] {4,9,4,127},
			new GoalEvent('+',
				new Goal(
					new Predicate("itemResource", new Term[] {
						new Variable(Type.STRING, "name",false),
						new Variable(Type.STRING, "cls",false)
					})
				)
			),
			new AND(
				new NOT(
					new Predicate("have", new Term[] {
						new Variable(Type.STRING, "name")
					})
				),
				new Predicate("artifact", new Term[] {
					Primitive.newPrimitive("base"),
					new Variable(Type.STRING, "baseName",false),
					new Variable(new ObjectType(cartago.ArtifactId.class), "baseId",false)
				})
			),
			new Block(
				"mams.javatypes.Passive", new int[] {4,126,13,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.STRING, "resName"),
						"mams.javatypes.Passive", new int[] {5,8,13,5},
						Operator.newOperator('+',
							new Variable(Type.STRING, "baseName"),
							Operator.newOperator('+',
								Primitive.newPrimitive("-"),
								new Variable(Type.STRING, "name")
							)
						)
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {6,8,6,137},
						new Predicate("makeArtifact", new Term[] {
							new Variable(Type.STRING, "resName"),
							Primitive.newPrimitive("mams.javatypes.artifacts.PassiveItemArtifact"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.STRING, "name"),
										new Variable(Type.STRING, "cls")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("mams.javatypes.Passive","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {7,8,7,50},
						new Predicate("linkArtifacts", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							Primitive.newPrimitive("out-1"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "baseId")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {8,8,8,25},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {9,8,9,44},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("createRoute", new Term[] {})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.javatypes.Passive", new int[] {10,8,13,5},
						new Predicate("artifact", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(Type.STRING, "resName"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					),
					new BeliefUpdate('+',
						"mams.javatypes.Passive", new int[] {11,8,13,5},
						new Predicate("itemResource", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(Type.STRING, "cls")
						})
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {12,8,12,49},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("getUri", new Term[] {
								new Variable(Type.STRING, "uri",false)
							})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
		addRule(new Rule(
			"mams.javatypes.Passive", new int[] {15,9,15,124},
			new GoalEvent('+',
				new Goal(
					new Predicate("listResource", new Term[] {
						new Variable(Type.STRING, "name",false),
						new Variable(Type.STRING, "cls",false)
					})
				)
			),
			new AND(
				new NOT(
					new Predicate("have", new Term[] {
						new Variable(Type.STRING, "name")
					})
				),
				new Predicate("artifact", new Term[] {
					Primitive.newPrimitive("base"),
					new Variable(Type.STRING, "baseName",false),
					new Variable(new ObjectType(cartago.ArtifactId.class), "id2",false)
				})
			),
			new Block(
				"mams.javatypes.Passive", new int[] {15,123,24,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.STRING, "resName"),
						"mams.javatypes.Passive", new int[] {16,8,24,5},
						Operator.newOperator('+',
							new Variable(Type.STRING, "baseName"),
							Operator.newOperator('+',
								Primitive.newPrimitive("-"),
								new Variable(Type.STRING, "name")
							)
						)
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {17,8,17,137},
						new Predicate("makeArtifact", new Term[] {
							new Variable(Type.STRING, "resName"),
							Primitive.newPrimitive("mams.javatypes.artifacts.PassiveListArtifact"),
							new ModuleTerm("cartago", new ObjectType(java.lang.Object[].class),
								new Predicate("params", new Term[] {
									new ListTerm(new Term[] {
										new Variable(Type.STRING, "name"),
										new Variable(Type.STRING, "cls")
									})
								}),
								new ModuleTermAdaptor() {
									public Object invoke(Intention intention, Predicate predicate) {
										return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).params(
											(astra.term.ListTerm) intention.evaluate(predicate.getTerm(0))
										);
									}
									public Object invoke(BindingsEvaluateVisitor visitor, Predicate predicate) {
										return ((astra.lang.Cartago) visitor.agent().getModule("mams.javatypes.Passive","cartago")).params(
											(astra.term.ListTerm) visitor.evaluate(predicate.getTerm(0))
										);
									}
								}
							),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {18,8,18,47},
						new Predicate("linkArtifacts", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							Primitive.newPrimitive("out-1"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id2")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {19,8,19,25},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {20,8,20,44},
						new Predicate("operation", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id"),
							new Funct("createRoute", new Term[] {})
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.javatypes.Passive", new int[] {22,8,24,5},
						new Predicate("artifact", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(Type.STRING, "resName"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					),
					new BeliefUpdate('+',
						"mams.javatypes.Passive", new int[] {23,8,24,5},
						new Predicate("listResource", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(Type.STRING, "cls")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"mams.javatypes.Passive", new int[] {26,9,26,71},
			new GoalEvent('+',
				new Goal(
					new Predicate("monitorPassiveItem", new Term[] {
						new Variable(Type.STRING, "name",false),
						new Variable(Type.STRING, "type",false),
						new Variable(Type.STRING, "qname",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"mams.javatypes.Passive", new int[] {26,70,32,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {27,8,27,60},
						new Predicate("lookupArtifact", new Term[] {
							new Variable(Type.STRING, "qname"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {28,8,28,25},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.javatypes.Passive", new int[] {31,8,32,5},
						new Predicate("artifact", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(Type.STRING, "qname"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					)
				}
			)
		));
		addRule(new Rule(
			"mams.javatypes.Passive", new int[] {34,9,34,116},
			new ModuleEvent("cartago",
				"$cse",
				new Predicate("signal", new Term[] {
					new Variable(Type.STRING, "source_artifact_name",false),
					new Funct("listItemArtifactCreated", new Term[] {
						new Variable(Type.STRING, "artifact_name",false),
						new Variable(Type.STRING, "type",false)
					})
				}),
				new ModuleEventAdaptor() {
					public Event generate(astra.core.Agent agent, Predicate predicate) {
						return ((astra.lang.Cartago) agent.getModule("mams.javatypes.Passive","cartago")).signal(
							predicate.getTerm(0),
							predicate.getTerm(1)
						);
					}
				}
			),
			Predicate.TRUE,
			new Block(
				"mams.javatypes.Passive", new int[] {34,115,43,5},
				new Statement[] {
					new Declaration(
						new Variable(Type.STRING, "name"),
						"mams.javatypes.Passive", new int[] {35,8,43,5},
						Operator.newOperator('+',
							new Variable(Type.STRING, "source_artifact_name"),
							Operator.newOperator('+',
								Primitive.newPrimitive("-"),
								new Variable(Type.STRING, "artifact_name")
							)
						)
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {36,8,36,59},
						new Predicate("lookupArtifact", new Term[] {
							new Variable(Type.STRING, "name"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id",false)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {37,8,37,25},
						new Predicate("focus", new Term[] {
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					),
					new BeliefUpdate('+',
						"mams.javatypes.Passive", new int[] {40,8,43,5},
						new Predicate("artifact", new Term[] {
							new Variable(Type.STRING, "artifact_name"),
							new Variable(Type.STRING, "name"),
							new Variable(new ObjectType(cartago.ArtifactId.class), "id")
						})
					),
					new BeliefUpdate('+',
						"mams.javatypes.Passive", new int[] {41,8,43,5},
						new Predicate("itemResource", new Term[] {
							new Variable(Type.STRING, "artifact_name"),
							new Variable(Type.STRING, "type")
						})
					),
					new Subgoal(
						"mams.javatypes.Passive", new int[] {42,8,43,5},
						new Goal(
							new Predicate("handleNewListItemResource", new Term[] {
								new Variable(Type.STRING, "artifact_name"),
								new Variable(Type.STRING, "type")
							})
						)
					)
				}
			)
		));
		addRule(new Rule(
			"mams.javatypes.Passive", new int[] {45,9,45,73},
			new GoalEvent('+',
				new Goal(
					new Predicate("handleNewListItemResource", new Term[] {
						new Variable(Type.STRING, "artifact_name",false),
						new Variable(Type.STRING, "type",false)
					})
				)
			),
			Predicate.TRUE,
			new Block(
				"mams.javatypes.Passive", new int[] {45,72,47,5},
				new Statement[] {
					new ModuleCall("cartago",
						"mams.javatypes.Passive", new int[] {46,8,46,95},
						new Predicate("println", new Term[] {
							Operator.newOperator('+',
								Primitive.newPrimitive("Unhandled list item resource: "),
								Operator.newOperator('+',
									new Variable(Type.STRING, "artifact_name"),
									Operator.newOperator('+',
										Primitive.newPrimitive(" of type: "),
										new Variable(Type.STRING, "type")
									)
								)
							)
						}),
						new DefaultModuleCallAdaptor() {
							public boolean inline() {
								return false;
							}

							public boolean invoke(Intention intention, Predicate predicate) {
								return ((astra.lang.Cartago) intention.getModule("mams.javatypes.Passive","cartago")).auto_action(intention,evaluate(intention,predicate));
							}
							public boolean suppressNotification() {
								return true;
							}
						}
					)
				}
			)
		));
	}

	public void initialize(astra.core.Agent agent) {
	}

	public Fragment createFragment(astra.core.Agent agent) throws ASTRAClassNotFoundException {
		Fragment fragment = new Fragment(this);
		return fragment;
	}

	public static void main(String[] args) {
		Scheduler.setStrategy(new TestSchedulerStrategy());
		ListTerm argList = new ListTerm();
		for (String arg: args) {
			argList.add(Primitive.newPrimitive(arg));
		}

		String name = java.lang.System.getProperty("astra.name", "main");
		try {
			astra.core.Agent agent = new Passive().newInstance(name);
			agent.initialize(new Goal(new Predicate("main", new Term[] { argList })));
			Scheduler.schedule(agent);
		} catch (AgentCreationException e) {
			e.printStackTrace();
		} catch (ASTRAClassNotFoundException e) {
			e.printStackTrace();
		};
	}
}
